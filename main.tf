resource "azurerm_private_dns_resolver" "resolver" {
  name                = var.name
  resource_group_name = var.resource_group_name
  location            = var.location
  virtual_network_id  = var.vnet_id
}

resource "azurerm_private_dns_resolver_inbound_endpoint" "inbound" {
  for_each                = var.inbound_endpoints
  name                    = each.key
  private_dns_resolver_id = azurerm_private_dns_resolver.resolver.id
  location                = var.location
  ip_configurations {
    subnet_id = each.value.subnet_id
  }
}

resource "azurerm_private_dns_resolver_outbound_endpoint" "outbound" {
  for_each                = var.outbound_endpoints
  name                    = each.key
  private_dns_resolver_id = azurerm_private_dns_resolver.resolver.id
  location                = var.location
  subnet_id               = each.value.subnet_id
}

data "azurerm_resource_group" "rg" {
  name = var.resource_group_name
}

resource "azurerm_private_dns_resolver_dns_forwarding_ruleset" "outboundRuleset" {
  for_each  = var.outbound_endpoints
  name                                       = each.key
  resource_group_name                        = var.resource_group_name
  location                                   = var.location
  private_dns_resolver_outbound_endpoint_ids = [azurerm_private_dns_resolver_outbound_endpoint.outbound[each.key].id]
}

locals {
  vnetlinks = flatten([
    for endpoint_key, endpoint_val in var.outbound_endpoints : [
      for vnet_id in toset(endpoint_val.vnet_ids) : {
        name      = endpoint_key
        rulesetId = azurerm_private_dns_resolver_dns_forwarding_ruleset.outboundRuleset[endpoint_key].id
        vnet_id   = vnet_id
      }
    ]
  ])
}

resource "azurerm_private_dns_resolver_virtual_network_link" "forwarningVNet" {
  for_each = {
    for val in local.vnetlinks : "${val.name}-${val.vnet_id}" => val
  }
  name                      = element(split("/", each.value.vnet_id), length(split("/", each.value.vnet_id)) - 1)
  dns_forwarding_ruleset_id = each.value.rulesetId
  virtual_network_id        = each.value.vnet_id
}

locals {
  rules = flatten([
    for endpoint_key, endpoint_val in var.outbound_endpoints : [
      for ruleset_name, ruleset_val in endpoint_val.forwardingRulesets : [
        for domain in toset(ruleset_val.domains) : {
          domain           = domain
          rulesetId        = azurerm_private_dns_resolver_dns_forwarding_ruleset.outboundRuleset[endpoint_key].id
          targetDnsServers = toset(ruleset_val.targetDnsServers)
        }
      ]
    ]
  ])
}

output "debug" {
  value = ""
}

resource "azurerm_private_dns_resolver_forwarding_rule" "rules" {
  for_each = {
    for val in local.rules : val.domain => val
  }

  name                      = trim(replace(replace(each.key, ".", "-"), "/[^a-zA-Z0-9-]/", ""), "-")
  dns_forwarding_ruleset_id = each.value.rulesetId
  domain_name               = each.key
  enabled                   = true

  dynamic "target_dns_servers" {
    for_each = toset(each.value.targetDnsServers)
    content {
      ip_address = target_dns_servers.value.ip_address
      port       = target_dns_servers.value.port
    }
  }
}
