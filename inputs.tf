variable "name" {
  type = string
}

variable "location" {
  type = string
}

variable "vnet_id" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "inbound_endpoints" {
  type = map(object({
    subnet_id = string
  }))
  default = {}
}

variable "outbound_endpoints" {
  type = map(object({
    subnet_id          = string
    vnet_ids           = list(string)
    forwardingRulesets = map(object({
      targetDnsServers = list(object({
        ip_address = string
        port      = number
      }))
      domains = list(string)
    }))
  }))
  default = {}
}
