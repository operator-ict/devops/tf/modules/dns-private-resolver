# Unreleased
## Added
## Changed
## Fixed

# [0.3.1] - 2023-02-20
## Changed
- Replaced AzureAPI by build-in terraform objects
